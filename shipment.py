# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from sql import Table
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.model import ModelView, fields


class InternalShipment(metaclass=PoolMeta):
    'Internal Shipment'
    __name__ = 'stock.shipment.internal'

    @classmethod
    def wait(cls, shipments):
        super(InternalShipment, cls).wait(shipments)
        cls.set_employee(shipments, 'shipped_by')

    @classmethod
    def done(cls, shipments):
        super(InternalShipment, cls).done(shipments)
        cls.set_employee(shipments, 'done_by')

    @classmethod
    def set_employee(cls, shipments, field):
        employee_id = Transaction().context.get('employee')
        if employee_id:
            cls.write(shipments, {field: employee_id})


class CreateInternalShipmentStart(ModelView):
    'Create Internal Shipment Start'
    __name__ = 'stock_duration.create_internal_shipment.start'
    from_location = fields.Many2One('stock.location', "From Location",
        required=True, domain=[
            ('type', 'in', ['view', 'storage', 'lost_found']),
        ])
    to_location = fields.Many2One('stock.location', "To Location",
        required=True, domain=[
            ('type', 'in', ['view', 'storage', 'lost_found']),
        ])


class CreateInternalShipment(Wizard):
    'Create Internal Shipment'
    __name__ = 'stock_duration.create_internal_shipment'
    start = StateView('stock_duration.create_internal_shipment.start',
        'stock_duration.create_internal_shipment_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Internal = pool.get('stock.shipment.internal')
        ShipmentSupplier = pool.get('stock.shipment.in')
        shipment_id = Transaction().context.get('active_id')
        shipment_supplier = ShipmentSupplier(shipment_id)

        moves_target = []
        today = date.today()

        for move in shipment_supplier.incoming_moves:
            moves_target.append({
                'product': move.product.id,
                'uom': move.product.default_uom.id,
                'quantity': move.quantity,
                'internal_quantity': move.quantity,
                'from_location': self.start.from_location.id,
                'to_location': self.start.to_location.id,
                'planned_date': today,
                'effective_date': today,
                'state': 'draft',
            })

        data = {
            'company': shipment_supplier.company.id,
            'effective_date': today,
            'planned_date': today,
            'effective_start_date': today,
            'planned_start_date': today,
            'from_location': self.start.from_location.id,
            'to_location': self.start.to_location.id,
            'state': 'draft',
            'moves': [('create', moves_target)],
        }
        Internal.create([data])
        return 'end'


class ShipmentOutForceDraft(Wizard):
    'Shipment Out Force Draft'
    __name__ = 'stock.shipment.out.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        id_ = Transaction().context['active_id']
        Shipment = Pool().get('stock.shipment.out')
        stock_move = Table('stock_move')
        if id_:
            shipment = Shipment(id_)
            cursor = Transaction().connection.cursor()
            for rec in shipment.inventory_moves:
                cursor.execute(*stock_move.delete(
                    where=stock_move.id == rec.id)
                )
            for rec in shipment.outgoing_moves:
                cursor.execute(*stock_move.update(
                    columns=[stock_move.state],
                    values=['draft'],
                    where=stock_move.id == rec.id)
                )
            shipment.state = 'draft'
            shipment.save()
        return 'end'


class ShipmentInternalForceDraft(Wizard):
    'Shipment Internal Force Draft'
    __name__ = 'stock.shipment.internal.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        id_ = Transaction().context['active_id']
        Shipment = Pool().get('stock.shipment.internal')
        stock_move = Table('stock_move')
        if id_:
            shipment = Shipment(id_)
            cursor = Transaction().connection.cursor()
            for rec in shipment.moves:
                cursor.execute(*stock_move.update(
                    columns=[stock_move.state],
                    values=['draft'],
                    where=stock_move.id == rec.id)
                )
            shipment.state = 'draft'
            shipment.save()
        return 'end'
