# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, timedelta
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction

STATES = {'invisible': (Eval('type') != 'goods')}

TIME_HISTORY = 90  # Days


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    stock_duration = fields.Function(fields.Integer('Stock Duration',
            states=STATES), 'get_stock_duration')
    stock_level = fields.Function(fields.Selection([
        ('point_order', 'Point Order'),
        ('overstock', 'Overstock'),
        ('exhausted', 'Exhausted'),
        ('normal', 'Normal'),
        ], 'Stock Level', states=STATES), 'get_stock_level')

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()

    def get_stock_duration(self, name):
        res = None
        #now = datetime.now()
        #laptime = now - timedelta(TIME_HISTORY)
        if Transaction().context.get('stock_date_end'):
            today = Transaction().context.get('stock_date_end')
        else:
            today = date.today()
        start_date = today - timedelta(TIME_HISTORY)
        Move = Pool().get('stock.move')
        # domain = ['AND', [('product', '=', self.id)],
        #                  [('create_date', '>=', laptime)],
        #                  [('state', '=', 'done')],
        #                  ['OR', ('to_location.type', '=', 'production'),
        #                         ('to_location.type', '=', 'customer')]]
        domain = [
            ('effective_date', '>=', start_date),
            ('effective_date', '<=', today),
            ('state', '=', 'done'),
            ('product', '=', self.id),
            ('to_location.type', 'in', ['customer', 'production']),
        ]
        if Transaction().context.get('locations'):
            domain.append(('from_location', 'in', Transaction().context.get('locations')))

        move_history_quantity = 0
        real_historical_time = None
        for move in Move.search(domain, order=[('effective_date', 'ASC')]):
            move_history_quantity += move.quantity
            if not real_historical_time:
                real_historical_time = (today-move.effective_date).days
        if move_history_quantity > 0:
            res = int((real_historical_time * self.quantity) / move_history_quantity)
        return res

    def get_stock_level(self, name):
        res = 'normal'
        #FIXME
        """
        if self.max_stock and self.quantity >= self.max_stock:
            res = 'overstock'
        elif self.min_stock and self.quantity <= self.min_stock:
            if self.quantity >= (self.min_stock * 0.5):
                res = 'point_order'
            else:
                res = 'exhausted'
        """
        return res
