# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
# from . import product
# from . import stock
# from . import shipment


def register():
    Pool.register(
        # product.Product,
        # stock.Move,
        # stock.MoveByProductStart,
        # shipment.CreateInternalShipmentStart,
        # shipment.InternalShipment,
        # stock.WarehouseStockStart,
        # stock.Inventory,
        # stock.ShipmentInternal,
        # stock.PrintProductsStart,
        module='stock_duration', type_='model')
    Pool.register(
        # stock.PrintMoveByProduct,
        # shipment.CreateInternalShipment,
        # stock.WarehouseStock,
        # stock.PrintProducts,
        # shipment.ShipmentOutForceDraft,
        # shipment.ShipmentInternalForceDraft,
        module='stock_duration', type_='wizard')
    Pool.register(
        # stock.MoveByProduct,
        # stock.WarehouseReport,
        # stock.PrintProductsReport,
        module='stock_duration', type_='report')
